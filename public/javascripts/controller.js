'use strict';
angular.module('cybermed.controller', ['ui.bootstrap'])
    .controller('cyberCtrl', function ($scope, $http, $log, $modal) {
        //$scope.pageClass = 'page-home';
        setTimeout(
            function(){
                $('.page').css('transform', 'translateX(0)');
                $('.page').css('-moz-transform', 'translateX(0)');
                $('.page').css('-webkit-transform', 'translateX(0)');
                $('.page').css('opacity','1');
                $('html,body').scrollTop(0);
            }
        ,0);
            
    
    })
    .controller('on3dCtrl', function ($scope, $http, $log, $modal) {
        $(document).ready(function(){
            window.scrollTo(0,0);
        });
        
        window.wow = (
            new WOW({
                mobile: false
            })
        ).init();
        
        $('#features-carousel').owlCarousel({
            rtl: modeRTL,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                }
            }
        });

        $scope.preSlideFea = function() {
            $('#features-carousel').data('owlCarousel').prev();
        };
        $scope.nextSlideFea = function() {
            $('#features-carousel').data('owlCarousel').next();
        };
    })
    .controller('in2Gtrl', function ($scope, $http, $log, $modal) {
        $(document).ready(function(){
            window.scrollTo(0,0);
        });
        
    })
    .controller('on3dlabCtrl', function ($scope, $http, $log, $modal) {
        $(document).ready(function(){
            window.scrollTo(0,0);
        });
        
    })
    .controller('surgicalCtrl', function ($scope, $http, $log, $modal) {
        $(document).ready(function(){
            window.scrollTo(0,0);
        });
        
    });