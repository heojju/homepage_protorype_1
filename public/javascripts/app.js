var app = angular.module('cybermed', ['ngRoute', ,'ngAnimate', 'cybermed.controller']);
app.config(function ($routeProvider, $locationProvider) {
    return $routeProvider
        .when('/', {
            templateUrl: 'partials/main.jade',
            controller: 'cyberCtrl'
        })
        .when('/ondemand3d-page', {
            templateUrl: 'partials/detailViews/ondemand3d.jade',
            controller: 'on3dCtrl'
        })
        .when('/in2guide-page', {
            templateUrl: 'partials/detailViews/in2guide.jade',
            controller: 'in2Gtrl'
        })
        .when('/on3dlab-page', {
            templateUrl: 'partials/detailViews/on3dlab.jade',
            controller: 'on3dlabCtrl'
        })
        .when('/surgical-page', {
            templateUrl: 'partials/detailViews/surgical.jade',
            controller: 'surgicalCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
});