var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', {
        title: 'Express'
    });
});
router.get('/partials/:name', function (req, res) {
    res.render('partials/' + req.params.name);
});
router.get('/partials/detailViews/:name', function (req, res) {
    res.render('partials/detailViews/' + req.params.name);
});


module.exports = router;
